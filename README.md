# Hello World con NodeJs - Express

	 Aplicación desarrollada con NodeJs y Express que contiene el servidor, las rutas para mostrar las vistas index ó users.

## Instalación

	Luego de realizar la descarga ó clonado del repositorio, se debe:
		* Posicionarse en la carpeta y aplicar por consola (Terminal) -> ´npm install´
		* Ejecutar servidor aplicando por consola (Terminal) -> ´node ./bin/www´ ó ´node app.js´
		* Abrir navegador y colocar la url -> ´localhost:3000´
		* Probar con la siguiente ruta -> ´localhost:3000/users´
		* Probar con otra ruta cualquiera -> ´localhost:3000/otraCualquiera´
		
