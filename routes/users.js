var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users', { notice: 'Vista que depende de la ruta USERS' });
});

module.exports = router;
